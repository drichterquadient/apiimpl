import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.0"
}

group = "apiImpl"
version = "1.0-SNAPSHOT"


subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    repositories {
        mavenCentral()
    }
    if (name.contains("impl")) {
        dependencies {
            implementation(project(path.replace("impl", "api")))
        }
    }
    dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))
    }
    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
    tasks {
        val cleanOut by creating(Delete::class) {
            setDelete("out")
        }
    }
}