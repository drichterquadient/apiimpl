package second.api

interface SecondaryStringProvider {
    fun provideString(): String
}