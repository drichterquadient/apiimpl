package second.impl

import org.springframework.stereotype.Component
import second.api.SecondaryStringProvider

@Component
class SecondaryStringProviderImpl : SecondaryStringProvider {

    override fun provideString(): String {
        return "secondary2"
    }
}