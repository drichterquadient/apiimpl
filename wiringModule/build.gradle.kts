plugins {
    application
}
dependencies {
    val thisProject = project
    rootProject.subprojects.filter { it.path.contains("api") }.forEach {
        val subProj = it
        thisProject.dependencies {
            implementation(project(subProj.path))
        }
    }
    rootProject.subprojects.filter { it.path.contains("impl") }.forEach {
        val subProj = it
        thisProject.dependencies {
            runtimeOnly(project(subProj.path))
        }
    }
    implementation( group= "org.springframework.boot", name= "spring-boot-starter", version= "2.0.6.RELEASE")
}
application {
    mainClassName = "wiring.WiringKt"
}


