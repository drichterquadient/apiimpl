package wiring

import first.api.StringProvider
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

fun main(args: Array<String>) {
    SpringApplication.run(Wiring::class.java)
}


@SpringBootApplication
@ComponentScan("first", "second")
open class Wiring(private val stringProvider: StringProvider): CommandLineRunner {
    override fun run(vararg args: String?) {
        println("===============================================")
        println(stringProvider.provideString())
        println("===============================================")
    }
}
