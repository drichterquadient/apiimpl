package first.impl

import first.api.StringProvider
import org.springframework.stereotype.Component
import second.api.SecondaryStringProvider

@Component
class StringProviderImpl(private val secondaryStringProvider: SecondaryStringProvider) : StringProvider {
    override fun provideString(): String {
        return secondaryStringProvider.provideString()
    }
}