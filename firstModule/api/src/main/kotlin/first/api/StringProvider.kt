package first.api

interface StringProvider {
    fun provideString(): String
}