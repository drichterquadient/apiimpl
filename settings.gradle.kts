rootProject.name = "apiImpl"

include(":firstModule-api", ":firstModule-impl", ":secondModule-api", ":secondModule-impl", ":wiringModule")
project(":firstModule-api").projectDir = File("$rootDir/firstModule/api")
project(":firstModule-impl").projectDir = File("$rootDir/firstModule/impl")
project(":secondModule-api").projectDir = File("$rootDir/secondModule/api")
project(":secondModule-impl").projectDir = File("$rootDir/secondModule/impl")
